import React from 'react';


//single bar
export const StatBar = props => {
    const widthBar = (props.current_value * 100) / props.max_value;

    const style = {
        barContainer: {
            height: '10px',
            border: '1px solid rgba(115, 115, 115, 60%)',
            borderRadius: '15px',
            padding: '2px',
        },
        bar: {
            backgroundColor: props.color,
            height: '100%',
            width: `${widthBar}%`,
            borderRadius: '15px',
        },
        label: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
        },
    };
    return ( 
        <>
            <div style={style.label}> 
                <span>{props.label}</span>
                <span>{props.current_value} /  {props.max_value}</span>
            </div>
            <div style={style.barContainer}>
                <div style={style.bar}></div>
            </div>
        </>
     );
};

//statsbars set
export const StatsBar = props => {

    return (
        <>
            <StatBar
                label = {'vie'}
                current_value = {props.current_health}
                max_value = {props.max_health}
                color = {'rgb(153, 255, 153)'}
            />
            <StatBar
                label = {'mana'}
                current_value = {props.current_mana}
                max_value = {props.max_mana}
                color = {'rgb(153, 204, 255)'}
            />
            <StatBar
                label = {'expérience'}
                current_value = {props.current_exp}
                max_value = {props.max_exp}
                color = {'rgb(204, 153, 255)'}
            />
        </>
    ) 
}