import React from 'react';


//single badge
export const StateBadge = props => {
    const state = props.state;
    const styles = {
        color: state.color,
        border: `2px solid ${state.color}`,
        borderRadius: '5px',
        padding: '2px',
        margin: '2px',
    };
    return (  
        <span style={styles}>
            {state.text} 
        </span>
    );
};

//badges list
export const StateBadgeList = props => {
    const statesList = props.statesList;
    return (
        <>
            {statesList.map( state =>
                <StateBadge state = {state}/>
            )}
        
        </>
    );
};