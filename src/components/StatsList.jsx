import React from 'react';

const StatsList = props => {
    const styles = {
        ul: {

        },
        li: {
            listStyleType: 'none',
        }
    }
    return (  
        <ul>
            <h3>Statistiques</h3>
            <li style={styles.li}> attaque: <span></span>{props.attack}</li>
            <li style={styles.li}> magie noire: {props.blackMagic}</li>
            <li style={styles.li}> magie blanche: {props.whiteMagic}</li>
            <li style={styles.li}> armure: {props.armor}</li>
            <li style={styles.li}> protection: {props.protection}</li>
            <li style={styles.li}> vitesse: {props.moveSpeed}</li>
        </ul>
    );
};

export default StatsList;
