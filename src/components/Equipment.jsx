import React from 'react';

const Equipment = props => {
    const styles = {
        ul: {

        },
        li: {
            listStyleType: 'none',
        }
    }
    return (  
        <ul>
            <h3>Equipement</h3>
            <li style={styles.li}>tête: {props.head}</li>
            <li style={styles.li}>corps: {props.body}</li>
            <li style={styles.li}>bras: {props.arms}</li>
            <li style={styles.li}>jambes: {props.legs}</li>
            <li style={styles.li}>pieds: {props.feet}</li>
            <li style={styles.li}>accessoire: {props.accessory}</li>
            <li style={styles.li}>arme: {props.weapon}</li>
            <li style={styles.li}>bouclier: {props.shield}</li>
        </ul>
    );
}
 
export default Equipment;