import React from 'react';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import HomePage from '../pages/HomePage';

const Routing = () => {
    return (  
        <Router>
            <Switch>
                <Route exact path='/' component={Home}/>
            </Switch>
        </Router>
    );
}

const Home = () => {
    return <HomePage/>
}

 
export default Routing;