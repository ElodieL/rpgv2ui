import React from 'react';
import Routing from './Routing';

const app_styles = {
  container: {
    display: 'flex',
    flexDirection: 'row',
    height: '100vh',
    justifyContent: 'center',
    marginLeft: '15px',
    marginRight: '15px',
    color: 'rgb(77, 77, 77)',
  }
}

function App() {
  return (
    <div style={app_styles.container}>
      <Routing/>
    </div>
  );
}

export default App;
