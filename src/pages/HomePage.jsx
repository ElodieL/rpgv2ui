import React from 'react';
import { StateBadgeList } from '../components/States';
import { POISON, PARALIZED, DEAD, BLESSED, PETRIFIED } from '../constants/STATES';
import { StatsBar } from '../components/StatsBar';
import StatsList from '../components/StatsList';
import Equipment from '../components/Equipment';

const HomePage = () => {
    const statesList = [ POISON, PARALIZED, DEAD, BLESSED, PETRIFIED ]
    return ( 
        <div>
            <h1>Home</h1>

            <StateBadgeList statesList = {statesList}/>

            <StatsBar
                current_exp = {10}
                max_exp = {100}
                current_health = {32}
                max_health = {100}
                current_mana = {60}
                max_mana = {100}
            />

            <StatsList
                attack = {15}
                blackMagic = {10}
                whiteMagic = {10}
                armor = {5}
                protection = {4}
                moveSpeed = {5}
            />

            <Equipment
                head = {'casque'}
                body = {'armure'}
                arms = {'gants de fer'}
                legs = {'jambière de fer'}
                feet = {'bottes de fer'}
                weapon = {'épée de fer'}
                shield = {'bouclier de fer'}
            />

        </div>
    );
}
 
export default HomePage;