export const POISON = {
    type: 'POISON',
    text: 'poison',
    color: 'rgb(204, 102, 255)',
}

export const BLESSED = {
    type: 'BLESSED',
    text: 'béni',
    color: 'rgb(102, 255, 204)',
}

export const PARALIZED = {
    type: 'PARALYZED',
    text: 'paralysé',
    color: 'rgb(255, 204, 102)',
}

export const DEAD = {
    type: 'DEAD',
    text: 'mort',
    color: 'rgb(64, 64, 64)',
}

export const PETRIFIED = {
    type: 'PETRIFIED',
    text: 'pétrifié',
    color: 'rgb(128, 128, 128)',
}

