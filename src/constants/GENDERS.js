export const FEMALE = {
    type: 'FEMALE',
    name: 'femme',
    symbol: '♀',
}
export const MALE = {
    type: 'MALE',
    name: 'homme',
    symbol: '♂',
}