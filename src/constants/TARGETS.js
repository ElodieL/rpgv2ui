export const MYSELF = 'MYSELF';
export const ENEMY = 'ENEMY';
export const ENEMIES = 'ENEMIES';
export const ALLY = 'ALLY';
export const ALLIES = 'ALLIES';
export const ALL = 'ALL';