import { DAMAGES, HEAL, STATE, STAT } from './EFFECTS';
import { ENEMY, ENEMIES, ALLY } from './TARGETS';
import { POISON } from './STATES';
import { ATTACK, WHITE_MAGIC, BLACK_MAGIC, MOVE_SPEED } from './STATS';

export const FIRST_AID = {
    id: 'FIRST_AID',
    name: 'premiers soins',
    type: WHITE_MAGIC,
    effect: HEAL,
    target: ALLY,
    value: 5,
}

export const FLAMM = {
    id: 'FLAMM',
    name : 'Flamme',
    type: BLACK_MAGIC,
    effect: DAMAGES,
    target: ENEMIES,
    value: 10,
}

export const SWORD_STRIKE = {
    id: 'FLAMM',
    name : 'Flamme',
    type: ATTACK,
    effect: DAMAGES,
    target: ENEMY,
    value: 10,
}

export const VENOM = {
    id: 'VENOM',
    name: 'Venin',
    type: BLACK_MAGIC,
    effect: STATE,
    state: POISON,
    target: ENEMY,
    value: 2,
}

export const EXHAUST = {
    id: 'EXHAUST',
    name: 'fatigue',
    type: BLACK_MAGIC,
    effect: STAT,
    stat: [ATTACK, MOVE_SPEED],
    target: ENEMY,
    value: [-5, -2]
}