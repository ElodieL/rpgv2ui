export const ATTACK = 'ATTACK';
export const BLACK_MAGIC = 'BLACK_MAGIC';
export const WHITE_MAGIC = 'WHITE_MAGIC';
export const ARMOR = 'ARMOR';
export const PROTECTION = 'PROTECTION';
export const MOVE_SPEED = 'MOVE_SPEED';