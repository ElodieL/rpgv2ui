import { HEAD } from './ITEM_TYPES';

export const HEADBAND = {
    id: 'HEADBAND',
    type: HEAD,
    name: 'bandeau',
    stats: {
        health: 0,
        mana: 10,
        attackDamages: 10,
        blackMagic: 5,
        whiteMagic: 20,
        armor: 10,
        protection: 5,
        moveSpeed: 5,
    },
    passive: '+1 mana en début de tour'
}