export const HEAD = 'HEAD';
export const BODY = 'BODY';
export const ARMS = 'ARMS';
export const LEGS = 'LEGS';
export const FEET = 'FEET';
export const ACCESSORY = 'ACCESSORY';
export const WEAPON = 'WEAPON';
export const SHIELD = 'SHIELD';